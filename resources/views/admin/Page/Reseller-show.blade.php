@extends(config('theme.back').'Template')
@section('title', 'Reseller')
@section('postscript')
    <script type="text/javascript" src="{{asset('plugins/masonry/masonry.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/script.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/lightbox-modal.js')}}"></script>
    <script type="text/javascript">
        $(function () {

            $(document).on('click', '.docThumb', function () {
                $(this).lightbox({modal: '#imagePopup'});
            });
        });

    </script>

    <script type="text/javascript">
        $("#resellerLogo").change(function () {
            readURL(this, 'resellerLogoImagePreview');
        });
    </script>

@endsection
@section('content')
    <section class="content">
        <div class="row">

            <div class="col-xs-12">
                <a href="/admin/resellers/{{$id}}/edit">
                    <button class="btn btn-success btn-sm pull-right"><i class="fa fa-user-plus"></i>Edit</button>
                </a>
                <br><br>
            </div>

            <div class="col-xs-12 col-sm-6">

                <div class="box box-white">
                    <div class="box-header">
                        <h3 class="box-title"><i class="fa fa-key"></i> Login Credentials</h3>
                    </div>
                    <div class="box-body">


                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                @if(!empty($errors->first("username")))
                                    <div class="form-group has-error">
                                        @else
                                            <div class="form-group">
                                                @endif
                                                <label>Username </label>
                                                <p placeholder="Username"
                                                   name="username"
                                                   value="{{ $info['username'] or old('username') }}"
                                                   readonly>{{ $info['username'] or old('username') }}</p>
                                            </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-6">
                                        @if(!empty($errors->first("password")))
                                            <div class="form-group has-error">
                                                @else
                                                    <div class="form-group">
                                                        @endif
                                                        {{--<label>Password</label>--}}
                                                        {{--<div class="input-group">--}}
                                                        {{--<span class="input-group-addon"><i--}}
                                                        {{--class="fa fa-key"></i></span>--}}
                                                        {{--<input type="password" class="form-control"--}}
                                                        {{--placeholder="Password" name="password"--}}
                                                        {{--value="dummypassword" readonly>--}}
                                                        {{--</div>--}}
                                                    </div>
                                            </div>
                                    </div>

                            </div>
                        </div>


                        <div class="box box-white">

                            <div class="box-header">
                                <h3 class="box-title"><i class="fa fa-industry"></i> Detail</h3>
                            </div>

                            <div class="box-body">

                                <div class="form-group">
                                    <label>Inc. Name</label>
                                    <p name="name"
                                       value="{{ $info['name']  or old('name') }}">{{$info['name']}}</p>
                                </div>

                                <div class="form-group">
                                    <label>Profile</label>
                                    <p name="profile"
                                       rows="6"
                                       readonly>{{ $info['profile']  or old('profile') }}</p>
                                </div>

                                <div class="form-group">
                                    <label>Logo</label>
                                    <img src="{{ $info['logo_preview']  or old('logo_preview') }}"
                                         id="resellerLogoImagePreview"
                                         class="img-responsive"
                                         style="max-width:200px;width: expression(this.width > 200 ? 200: true);height: auto;">
                                </div>


                            </div>
                        </div>

                        <div class="box box-white">

                            <div class="box-header">
                                <h3 class="box-title"><i class="fa fa-industry"></i> Detail</h3>
                            </div>

                            <div class="box-body">


                                <label>Attachments</label>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <select hidden multiple="multiple" id="selected-image-names"
                                                name="selectedImageNames[]">
                                            @if(isset($info['attachments']))
                                                @foreach($info['attachments']  as $attachment)
                                                    <option selected value="{{$attachment['name']}}">
                                                        My
                                                        option
                                                    </option>
                                                @endforeach
                                            @else
                                                @if(old('selectedImageNames', null) != null)
                                                    @foreach(old('selectedImageNames')  as $selectedImageName)
                                                        <option selected value="{{$selectedImageName}}">
                                                            My
                                                            option
                                                        </option>
                                                    @endforeach
                                                @endif
                                            @endif
                                        </select>
                                        <div class="col-xs-9 no-padding">
                                            <select hidden multiple="multiple" id="selected-images"
                                                    name="selectedImages[]">
                                                @if(isset($info['attachments']))
                                                    @foreach($info['attachments']  as $attachment)
                                                        <option selected value="{{$attachment['image']}}">
                                                            My
                                                            option
                                                        </option>
                                                    @endforeach
                                                @else
                                                    @if(old('selectedImages', null) != null)
                                                        @foreach(old('selectedImages')  as $selectedImage)
                                                            <option selected value="{{$selectedImage}}">My
                                                                option
                                                            </option>
                                                        @endforeach
                                                    @endif
                                                @endif


                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="gallery wall" id="gallery-items">
                                    @if(isset($info['attachments']))
                                        @foreach($info['attachments']  as $attachment)
                                            <div class="imageThumbList bg-gray">
                                                <img src="{{$attachment['image']}}">
                                                <span data-src="{{$attachment['image']}}"
                                                      class="pointer docThumb">{{$attachment['name']}}</span>
                                                <i class="fa fa-times-circle text-danger"
                                                   onclick="removeItem(this)"></i>
                                            </div>
                                        @endforeach
                                    @else
                                        @if(old('selectedImages', null) != null)
                                            @foreach(old('selectedImages')  as $key => $selectedImage)
                                                <div class="imageThumbList bg-gray">
                                                    <img src="{{$selectedImage}}">
                                                    <span data-src="{{$selectedImage}}"
                                                          class="pointer docThumb">{{old('selectedImageNames')[$key]}}</span>
                                                    <i class="fa fa-times-circle text-danger"
                                                       onclick="removeItem(this)"></i>
                                                </div>
                                            @endforeach
                                        @endif
                                    @endif


                                </div>

                            </div>
                        </div>


                    </div>


                    <div class="col-xs-12 col-sm-6">
                        <div class="box box-white">
                            <div class="box-header">
                                <h3 class="box-title"><i class="fa fa-address-book"></i> Contact Info</h3>
                            </div>
                            <div class="box-body">

                                <div class="">
                                    <label>Contact Person</label>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="form-group">
                                                <p name="first_name"
                                                   placeholder="First Name"
                                                   value="{{$info['first_name']  or old('first_name') }}">{{$info['first_name'].' '.$info['middle_name'].' '. $info['last_name']}}</p>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label>Address</label>
                                        <p>{{$info['address']  or old('address') }}</p>
                                    </div>

                                    <div class="form-group">
                                        <label>Contact</label>
                                        <div class="row">
                                            <div class="col-xs-6">
                                                @if(!empty($errors->first("phone_no")))
                                                    <div class="input-group form-group has-error">
                                                        @else
                                                            <div class="input-group form-group">
                                                                @endif
                                                                <span class="input-group-addon"><i
                                                                            class="fa fa-phone"></i></span>
                                                                <input type="text"
                                                                       class="form-control"
                                                                       placeholder="Phone No."
                                                                       name="phone_no"
                                                                       value="{{$info['phone_no']  or old('phone_no') }}"
                                                                       readonly>
                                                            </div>
                                                    </div>
                                                    <div class="col-xs-6">
                                                        @if(!empty($errors->first("mobile_no")))
                                                            <div class="input-group form-group has-error">
                                                                @else
                                                                    <div class="input-group form-group">
                                                                        @endif
                                                                        <span class="input-group-addon"><i
                                                                                    class="fa fa-mobile-alt"></i></span>
                                                                        <input type="text"
                                                                               class="form-control"
                                                                               placeholder="Mobile No."
                                                                               name="mobile_no"
                                                                               value="{{$info['mobile_no']  or old('mobile_no') }}"
                                                                               readonly>
                                                                    </div>
                                                            </div>
                                                    </div>
                                            </div>

                                            @if(!empty($errors->first("email")))
                                                <div class="form-group has-error">
                                                    @else
                                                        <div class="form-group">
                                                            @endif
                                                            <label>Email</label>
                                                            <div class="input-group">
                                                    <span class="input-group-addon"><i
                                                                class="fa fa-envelope"></i></span>
                                                                <input type="email"
                                                                       class="form-control"
                                                                       placeholder="Email"
                                                                       name="email"
                                                                       value="{{$info['email']  or old('email') }}"
                                                                       readonly>
                                                            </div>
                                                        </div>

                                                        @if(!empty($errors->first("website")))
                                                            <div class="form-group has-error">
                                                                @else
                                                                    <div class="form-group">
                                                                        @endif
                                                                        <label>Website</label>
                                                                        <div class="input-group">
                                                                                                                <span class="input-group-addon"><i
                                                                                                                            class="fa fa-globe"></i></span>
                                                                            <input type="url"
                                                                                   class="form-control"
                                                                                   placeholder="Website"
                                                                                   name="website"
                                                                                   readonly
                                                                                   value="{{$info['website']  or old('website') }}">
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group">
                                                                        <label>GPS
                                                                            Location</label>
                                                                        <div class="input-group">
                                                    <span class="input-group-addon"><i
                                                                class="fa fa-map-marker"></i></span>
                                                                            <input type="text"
                                                                                   class="form-control"
                                                                                   placeholder="Latitude"
                                                                                   name="latitude"
                                                                                   readonly>
                                                                            <input type="text"
                                                                                   class="form-control"
                                                                                   placeholder="Longitude"
                                                                                   name="longitude"
                                                                                   readonly>
                                                                        </div>
                                                                    </div>

                                                            </div>
                                                </div>

                                        </div>

                                    </div>


                                    </form>

    </section>

    <div class="modal fade in" id="imagePopup">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h6 class="modal-title">&nbsp;</h6>
                </div>
                <div class="modal-body text-center">
                    <img>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection
