@extends(config('theme.back').'Template')
@section('title', 'Dashboard')
@section('postscript')
    <link href="{{asset('plugins/morris/morris.css')}}" type="text/css" rel="stylesheet">
    <script src="{{asset('plugins/chartjs/Chart.js')}}" type="application/javascript"></script>
    <script src="{{asset('plugins/raphael/raphael.js')}}" type="application/javascript"></script>
    <script src="{{asset('plugins/morris/morris.js')}}" type="application/javascript"></script>
    <script type="application/javascript">
        // -------------
        // - PIE CHART -
        // -------------
        // Get context with jQuery - using jQuery's .get() method.
        var pieChartCanvas = $('#pieChart').get(0).getContext('2d');
        var pieChart       = new Chart(pieChartCanvas);
        var PieData        = [
            {
                value    : 700,
                color    : '#f56954',
                highlight: '#f56954',
                label    : 'Wlink'
            },
            {
                value    : 500,
                color    : '#00a65a',
                highlight: '#00a65a',
                label    : 'Vianet'
            },
            {
                value    : 400,
                color    : '#f39c12',
                highlight: '#f39c12',
                label    : 'HONS'
            },
            {
                value    : 600,
                color    : '#00c0ef',
                highlight: '#00c0ef',
                label    : 'Reseller 1'
            },
            {
                value    : 300,
                color    : '#3c8dbc',
                highlight: '#3c8dbc',
                label    : 'Reseller 2'
            },
            {
                value    : 100,
                color    : '#d2d6de',
                highlight: '#d2d6de',
                label    : 'Reseller 3'
            }
        ];
        var pieOptions     = {
            // Boolean - Whether we should show a stroke on each segment
            segmentShowStroke    : true,
            // String - The colour of each segment stroke
            segmentStrokeColor   : '#fff',
            // Number - The width of each segment stroke
            segmentStrokeWidth   : 1,
            // Number - The percentage of the chart that we cut out of the middle
            percentageInnerCutout: 50, // This is 0 for Pie charts
            // Number - Amount of animation steps
            animationSteps       : 100,
            // String - Animation easing effect
            animationEasing      : 'easeOutBounce',
            // Boolean - Whether we animate the rotation of the Doughnut
            animateRotate        : true,
            // Boolean - Whether we animate scaling the Doughnut from the centre
            animateScale         : false,
            // Boolean - whether to make the chart responsive to window resizing
            responsive           : true,
            // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
            maintainAspectRatio  : false,
            // String - A legend template


  };
  pieChart.Doughnut(PieData, pieOptions);



  /** LINE CHART **/
  var line = new Morris.Line({
            element          : 'salesChart',
            resize           : true,
            data             : [
                { y: '2011 Q1', item1: 2666 },
                { y: '2011 Q2', item1: 2778 },
                { y: '2011 Q3', item1: 4912 },
                { y: '2011 Q4', item1: 3767 },
                { y: '2012 Q1', item1: 6810 },
                { y: '2012 Q2', item1: 5670 },
                { y: '2012 Q3', item1: 4820 },
                { y: '2012 Q4', item1: 15073 },
                { y: '2013 Q1', item1: 10687 },
                { y: '2013 Q2', item1: 8432 }
            ],
            xkey             : 'y',
            ykeys            : ['item1'],
            labels           : ['Item 1'],
            lineColors       : ['#00c0ef'],
            lineWidth        : 1,
            hideHover        : 'auto',
            gridTextColor    : '#999',
            gridStrokeWidth  : 0.4,
            pointSize        : 4,
            pointStrokeColors: ['#efefef'],
            gridLineColor    : '#efefef',
            gridTextFamily   : 'Open Sans',
            gridTextSize     : 10
        });
    </script>
@endsection
@section('content')
    <section class="content">
        <div class="row">
            <div class="col-xs-12 col-sm-9">

                <div class="row">
                    <div class="col-12 col-sm-3">
                        <div class="info-box bg-white">
                            <span class="info-box-icon"><i class="fa fa-user-check text-green"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Active Subscribers</span>
                                <span class="info-box-number text-black">41,410</span>

                                <div class="progress bg-green">
                                    <div class="progress-bar" style="width: 70%"></div>
                                </div>
                                <span class="progress-description">
                    <i class="fa fa-caret-up text-green"></i> 7% in 30 Days
                  </span>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-3">
                        <div class="info-box bg-white">
                            <span class="info-box-icon"><i class="fas fa-user-plus text-aqua"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">New Subscribers</span>
                                <span class="info-box-number text-black">1,410</span>

                                <div class="progress bg-aqua">
                                    <div class="progress-bar" style="width: 70%"></div>
                                </div>
                                <span class="progress-description">
                     <i class="fa fa-caret-down text-red"></i> 2% than last mth
                  </span>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-3">
                        <div class="info-box bg-white">
                            <span class="info-box-icon"><i class="fa fa-store text-purple"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Resellers</span>
                                <span class="info-box-number text-black">1,410</span>

                                <div class="progress bg-purple">
                                    <div class="progress-bar" style="width: 70%"></div>
                                </div>
                                <span class="progress-description">
                    <i class="fa fa-caret-up text-green"></i> 1% in 30 Days
                  </span>
                            </div>
                        </div>
                    </div>

                    <div class="col-12 col-sm-3">
                        <div class="info-box bg-white">
                            <span class="info-box-icon"><i class="fas fa-tv text-teal"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Channels</span>
                                <span class="info-box-number text-black">110</span>

                                <div class="progress bg-teal">
                                    <div class="progress-bar" style="width: 70%"></div>
                                </div>
                                <span class="progress-description">
                    <i class="fa fa-caret-right text-primary"></i> in 30 Days
                  </span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="box box-white">
                    <div class="box-header">
                        <h3 class="box-title"><i class="fa fa-chart-line"></i> Sales Chart</h3>
                    </div>
                    <div class="box-body bg-white">
                        <div id="salesChart" class="chart"></div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-xs-12">
                        <div class="box box-white">
                            <div class="box-header">
                                <h3 class="box-title"><i class="fa fa-table"></i> Table data</h3>
                            </div>
                            <div class="box-body no-padding">
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>SN</th>
                                    <th>SN</th>
                                    <th>SN</th>
                                    <th>SN</th>
                                    <th>SN</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>SN</td>
                                    <td>SN</td>
                                    <td>SN</td>
                                    <td>SN</td>
                                    <td>SN</td>
                                </tr>
                                <tr>
                                    <td>SN</td>
                                    <td>SN</td>
                                    <td>SN</td>
                                    <td>SN</td>
                                    <td>SN</td>
                                </tr>
                                <tr>
                                    <td>SN</td>
                                    <td>SN</td>
                                    <td>SN</td>
                                    <td>SN</td>
                                    <td>SN</td>
                                </tr>
                                <tr>
                                    <td>SN</td>
                                    <td>SN</td>
                                    <td>SN</td>
                                    <td>SN</td>
                                    <td>SN</td>
                                </tr>
                            </tbody>
                        </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">

                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="box box-white">
                            <div class="box-header">
                                <h3 class="box-title"><i class="fab fa-mixcloud"></i> Top Channels</h3>
                            </div>
                            <div class="box-body no-padding">
                                <ul class="nav nav-stacked">
                                    <li><a href="#">Star Sports HD <span class="pull-right fab fa-google text-lime"></span></a></li>
                                    <li><a href="#">Colors HD <span class="pull-right fa fa-globe text-green"></span></a></li>
                                    <li><a href="#">Star Plus <span class="pull-right fa fa-star text-blue"></span></a></li>
                                    <li><a href="#">Zee TV HD <span class="pull-right fab fa-telegram-plane text-aqua"></span></a></li>
                                    <li><a href="#">AP1 HD <span class="pull-right fab fa-apple text-teal"></span></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="box box-white">
                            <div class="box-header">
                                <h3 class="box-title"><i class="fa fa-video"></i> Top Shows</h3>
                            </div>
                            <div class="box-body no-padding">
                                <ul class="nav nav-stacked">
                                    <li><a href="#">Show 1 <span class="pull-right badge bg-blue">30081</span></a></li>
                                    <li><a href="#">Show 2 <span class="pull-right badge bg-aqua">25045</span></a></li>
                                    <li><a href="#">Show 3 <span class="pull-right badge bg-green">21224</span></a></li>
                                    <li><a href="#">Show 4 <span class="pull-right badge bg-red">18429</span></a></li>
                                    <li><a href="#">Show 5 <span class="pull-right badge bg-red">18120</span></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <div class="box box-white">
                            <div class="box-header">
                                <h3 class="box-title"><i class="fa fa-film"></i> Most Viewed Movies</h3>
                            </div>
                            <div class="box-body no-padding">
                                <ul class="nav nav-stacked">
                                    <li><a href="#">Deadpool <span class="pull-right badge bg-blue">5031</span></a></li>
                                    <li><a href="#">Bahubali <span class="pull-right badge bg-aqua">5009</span></a></li>
                                    <li><a href="#">Ice Age 2 <span class="pull-right badge bg-green">3412</span></a></li>
                                    <li><a href="#">Avengers <span class="pull-right badge bg-red">2842</span></a></li>
                                    <li><a href="#">Three Idiots <span class="pull-right badge bg-red">2842</span></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>

            </div>

            <div class="col-xs-12 col-sm-3">
                <div class="box box-white">
                    <div class="box-header with-border">
                        <h3 class="box-title">Reseller Statistics</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                            </button>
                        </div>
                    </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="chart-responsive">
                                        <canvas id="pieChart" height="155" width="258" style="width: 258px; height: 155px;"></canvas>
                                    </div>
                                    <!-- ./chart-responsive -->
                                </div>
                                <!-- /.col -->
                                <div class="col-md-4">
                                    <ul class="chart-legend clearfix text-sm">
                                        <li><i class="fa fa-circle text-red"></i> Wlink</li>
                                        <li><i class="fa fa-circle text-green"></i> Vianet</li>
                                        <li><i class="fa fa-circle text-yellow"></i> Hons</li>
                                        <li><i class="fa fa-circle text-aqua"></i> Reseller1</li>
                                        <li><i class="fa fa-circle text-light-blue"></i> Reseller</li>
                                        <li><i class="fa fa-circle text-gray"></i> Reseller3</li>
                                    </ul>
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.box-body -->
                        <!-- /.footer -->

                </div>

                <div class="box box-white">
                    <div class="box-header with-border">
                        <h4 class="box-title">Timeline</h4>
                    </div>
                    <div class="box-body" style="height: 400px;overflow-y: auto;">
                        <ul class="products-list product-list-in-box">
                            <li class="item with-border"><span class="fa fa-user-plus text-aqua"></span> Subscriber <a href="">viewer101</a> added by <a href="">Reseller</a> </li>
                            <li class="item with-border"><span class="fa fa-archive text-aqua"></span> <a href="">SportsHD</a> Package created by <a href="">Reseller</a> </li>
                            <li class="item with-border"><span class="fa fa-tv text-green"></span> <a href="">Discovery</a> Channel enabled by <a href="">Admin</a> </li>
                            <li class="item with-border"><span class="fa fa-tv text-aqua"></span> <a href="">Discovery</a> Channel added by <a href="">Admin</a> </li>
                            <li class="item with-border"><span class="fa fa-user-minus text-red"></span> Subscriber <a href="">viewer011</a> removed by <a href="">Reseller</a> </li>
                            <li class="item with-border"><span class="fa fa-user-plus text-aqua"></span> Subscriber <a href="">viewer001</a> added by <a href="">Reseller</a> </li>
                            <li class="item with-border"><span class="fa fa-user-plus text-aqua"></span> Subscriber <a href="">viewer001</a> added by <a href="">Reseller</a> </li>
                            <li class="item with-border"><span class="fa fa-user-plus text-aqua"></span> Subscriber <a href="">viewer001</a> added by <a href="">Reseller</a> </li>
                            <li class="item with-border"><span class="fa fa-user-plus text-aqua"></span> Subscriber <a href="">viewer001</a> added by <a href="">Reseller</a> </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection