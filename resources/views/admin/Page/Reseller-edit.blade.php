@extends(config('theme.back').'Template')
@section('title', 'Edit Reseller')
@section('postscript')
    <script type="text/javascript" src="{{asset('plugins/masonry/masonry.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/script.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/lightbox-modal.js')}}"></script>
    <script type="text/javascript">
        $(".required").after(" <span style='color: red'><b>*</b></span>");

        $(function () {

            $(document).on('click', '.docThumb', function () {
                $(this).lightbox({modal: '#imagePopup'});
            });
        });

        $("#password").on('click',function(){
            if($("#password").attr("readonly")){
                var id = 1;
                $('#modal-edit-password').modal('show');
                $('.confirm-edit').val(id);
            }

        });
        $('.confirm-edit').click(function(){
            $("#password").removeAttr("readonly");
            $('#modal-edit-password').modal('hide');
        });

    </script>

    <script type="text/javascript">
        $("#resellerLogo").change(function () {
            readURL(this, 'resellerLogoImagePreview');
        });
    </script>
    <script>
        $(document).ready(function () {

            $("#uploadDocument").on('click', function (event) {
                var config = {!! json_encode($config) !!};
                var selectedDOc = $('#doc-names option:selected');
                var name;
                var image;
                var values = $.map(selectedDOc, function (option) {
                    name = option.value;
                    // alert(name);
                });
                // alert(name);
                var property = document.getElementById("document");
                if (property.value) {
                    property = property.files[0];
                    var image_name = property.name;
                    var image_extention = image_name.split('.').pop().toLowerCase();
                    if (jQuery.inArray(image_extention, ["gif", "jpg", "jpeg", "png"]) == -1) {
                        document.getElementById("document").value = '';
                        $('div#image-alert').html('<br><div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> <i class="icon fa fa-check"></i> Invalid image file. </div>');
                    }
                    else if(property.size > 1048576){
                        $('div#image-alert').html('<br><div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> <i class="icon fa fa-check"></i> File size exceded. </div>');

                    }else {
                        var FR = new FileReader();

                        FR.addEventListener("load", function (e) {
                            console.log(e.target.result);
                            $("#selected-images").append('<option selected value="' + e.target.result + '">My option</option>');
                        });

                        FR.readAsDataURL(property);

                        image = URL.createObjectURL(property);
                        $("#selected-image-names").append('<option selected value="' + name + '">My option</option>');

                        $('#gallery-items').append(
                            '<div class="imageThumbList bg-gray">\n' +
                            '<img src="' + image + '">\n' +
                            '<span data-src="' + image + '" class="pointer docThumb">' + name + '</span>\n' +
                            '<i class="fa fa-times-circle text-danger" onclick="removeItem(this)"></i>\n' +
                            '</div>'
                        );
                    }
                    document.getElementById("document").value = '';



                }
                event.preventDefault();

            });
        });

        function removeItem(dom) {
            var name = $(dom).parent().children(".pointer").text();
            var parent = $(dom).parent();
            var image = $(dom).parent().children("img").attr("src");
            var selectedNames = document.getElementById("selected-image-names");
            var selectedImages = document.getElementById("selected-images");
            for (var i = 0; i < selectedNames.length; i++) {
                if (selectedNames.options[i].value == name && selectedImages.options[i].value == image) {
                    selectedNames.remove(i);
                    selectedImages.remove(i);
                    parent.remove();
                    // refreshMasonry();
                }
                // now have option.text, option.value
            }
            event.preventDefault();
        }

    </script>
@endsection
@section('content')
    <section class="content">
        <div id="delete-alert" class="col-xs-12"></div>
        <form method="post" action="{{ $action or "" }}" enctype="multipart/form-data">
            <input type="hidden" name="_method" value="PUT"/>
            <input type="hidden" name="_token" value="{{csrf_token()}}"/>
            <div class="row">

                <div class="col-xs-12">
                    <button class="btn btn-success btn-sm pull-right"><i class="fa fa-user-plus"></i> Save</button>
                    <br><br>
                </div>

                <div class="col-xs-12 col-sm-6">

                    <div class="box box-white">
                        <div class="box-header">
                            <h3 class="box-title"><i class="fa fa-key"></i> Login Credentials</h3>
                        </div>
                        <div class="box-body">


                            <div class="row">
                                <div class="col-xs-12 col-sm-6">
                                    @if(!empty($errors->first("username")))
                                        <div class="form-group has-error">
                                            @else
                                                <div class="form-group">
                                                    @endif
                                                    <label class="required">Username </label>
                                                    <div class="input-group">
                                                        <span class="input-group-addon"><i
                                                                    class="fa fa-user"></i></span>
                                                        <input type="text" class="form-control" placeholder="Username"
                                                               name="username"
                                                               value="{{old('username')?old('username'):$info['username'] }}">
                                                    </div>
                                                </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-6">
                                            @if(!empty($errors->first("password")))
                                                <div class="form-group has-error">
                                                    @else
                                                        <div class="form-group">
                                                            @endif
                                                            <label class="required">Password</label>
                                                            <div class="input-group">
                                                                <span class="input-group-addon"><i
                                                                            class="fa fa-key"></i></span>
                                                                <input readonly type="text" class="form-control"
                                                                       placeholder="Password" name="password" id="password"
                                                                       value="{{old('password')?old('password'):$info['password'] }}">
                                                            </div>
                                                        </div>
                                                </div>
                                        </div>

                                </div>
                            </div>


                            <div class="box box-white">

                                <div class="box-header">
                                    <h3 class="box-title"><i class="fa fa-industry"></i> Detail</h3>
                                </div>

                                <div class="box-body">

                                    @if(!empty($errors->first("name")))
                                        <div class="form-group has-error">
                                            @else
                                                <div class="form-group">
                                                    @endif
                                                    <label class="required">Inc. Name</label>
                                                    <input class="form-control" name="name"
                                                           value="{{ old('name')?old('name'):$info['name'] }}">
                                                </div>

                                                @if(!empty($errors->first("profile")))
                                                    <div class="form-group has-error">
                                                        @else
                                                            <div class="form-group">
                                                                @endif
                                                                <label class="required">Profile</label>
                                                                <textarea class="form-control" name="profile"
                                                                          rows="6">{{old('profile')?old('profile'):$info['profile'] }}</textarea>
                                                            </div>

                                                            <div class="form-group {{$errors->has("logo")?'has-error':''}}">
                                                                <label>Logo</label>
                                                                <input type="file" id="resellerLogo"
                                                                       class="form-control" name="logo">
                                                                <img src="{{old('logo_preview')?old('logo_preview'):$info['logo']}}"
                                                                     id="resellerLogoImagePreview"
                                                                     class="img-responsive"
                                                                     style="max-width:200px;width: expression(this.width > 200 ? 200: true);height: auto;">
                                                                <input type="hidden" name="logo_preview" value="{{ old('logo_preview') }}">

                                                            </div>


                                                    </div>
                                        </div>

                                        <div class="box box-white">

                                            <div class="box-header">
                                                <h3 class="box-title"><i class="fa fa-industry"></i> Detail</h3>
                                            </div>

                                            <div class="box-body">

                                                <div class="form-group {{$errors->has("details")?'has-error':''}}">
                                                    <label class="required">Attachments</label>
                                                    <div class="row">
                                                        <div class="col-xs-12">
                                                            <div class="col-xs-3 no-padding">
                                                                <select class="form-control bg-gray-light"
                                                                        id="doc-names">
                                                                    <option value="contract">Contract</option>
                                                                    <option value="license">License</option>
                                                                    <option value="citizenship">Citizenship</option>
                                                                </select>
                                                            </div>
                                                            <select hidden multiple="multiple" id="selected-image-names"
                                                                    name="selectedImageNames[]">
                                                                @if(isset($info['attachments']))
                                                                    @foreach($info['attachments']  as $attachment)
                                                                        <option selected
                                                                                value="{{$attachment['name']}}">
                                                                            My
                                                                            option
                                                                        </option>
                                                                    @endforeach
                                                                @else
                                                                    @if(old('selectedImageNames', null) != null)
                                                                        @foreach(old('selectedImageNames')  as $selectedImageName)
                                                                            <option selected
                                                                                    value="{{$selectedImageName}}">
                                                                                My
                                                                                option
                                                                            </option>
                                                                        @endforeach
                                                                    @endif
                                                                @endif
                                                            </select>
                                                            <div class="col-xs-9 no-padding">
                                                                <div class="input-group">
                                                                    <input type="file" id="document"
                                                                           class="form-control">
                                                                    <div class="input-group-btn">
                                                                        <button type="button" id="uploadDocument"
                                                                                class="btn btn-primary"><i
                                                                                    class="fa fa-upload"></i> Upload
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                                <select hidden multiple="multiple" id="selected-images"
                                                                        name="selectedImages[]">
                                                                    @if(isset($info['attachments']))
                                                                        @foreach($info['attachments']  as $attachment)
                                                                            <option selected
                                                                                    value="{{$attachment['image']}}">
                                                                                My
                                                                                option
                                                                            </option>
                                                                        @endforeach
                                                                    @else
                                                                        @if(old('selectedImages', null) != null)
                                                                            @foreach(old('selectedImages')  as $selectedImage)
                                                                                <option selected
                                                                                        value="{{$selectedImage}}">My
                                                                                    option
                                                                                </option>
                                                                            @endforeach
                                                                        @endif
                                                                    @endif


                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="gallery wall" id="gallery-items">
                                                    @if(isset($info['attachments']))
                                                        @foreach($info['attachments']  as $attachment)
                                                            <div class="imageThumbList bg-gray">
                                                                <img src="{{$attachment['image']}}">
                                                                <span data-src="{{$attachment['image']}}"
                                                                      class="pointer docThumb">{{$attachment['name']}}</span>
                                                                <i class="fa fa-times-circle text-danger"
                                                                   onclick="removeItem(this)"></i>
                                                            </div>
                                                        @endforeach
                                                    @else
                                                        @if(old('selectedImages', null) != null)
                                                            @foreach(old('selectedImages')  as $key => $selectedImage)
                                                                <div class="imageThumbList bg-gray">
                                                                    <img src="{{$selectedImage}}">
                                                                    <span data-src="{{$selectedImage}}"
                                                                          class="pointer docThumb">{{old('selectedImageNames')[$key]}}</span>
                                                                    <i class="fa fa-times-circle text-danger"
                                                                       onclick="removeItem(this)"></i>
                                                                </div>
                                                            @endforeach
                                                        @endif
                                                    @endif


                                                </div>

                                            </div>
                                        </div>


                                </div>


                                <div class="col-xs-12 col-sm-6">
                                    <div class="box box-white">
                                        <div class="box-header">
                                            <h3 class="box-title"><i class="fa fa-address-book"></i> Contact Info</h3>
                                        </div>
                                        <div class="box-body">

                                            <div class="">
                                                <label class="required">Contact Person</label>
                                                <div class="row">
                                                    <div class="col-xs-4">
                                                        @if(!empty($errors->first("first_name")))
                                                            <div class="form-group has-error">
                                                                @else
                                                                    <div class="form-group">
                                                                        @endif
                                                                        <input class="form-control" name="first_name"
                                                                               placeholder="First Name"
                                                                               value="{{$info['first_name']  or old('first_name') }}">
                                                                    </div>
                                                            </div>
                                                            <div class="col-xs-4 no-padding">
                                                                @if(!empty($errors->first("middle_name")))
                                                                    <div class="form-group has-error">
                                                                        @else
                                                                            <div class="form-group">
                                                                                @endif
                                                                                <input class="form-control"
                                                                                       name="middle_name"
                                                                                       placeholder="Middle Name"
                                                                                       value="{{$info['middle_name']  or old('middle_name') }}"
                                                                                >
                                                                            </div>
                                                                    </div>
                                                                    <div class="col-xs-4">
                                                                        @if(!empty($errors->first("last_name")))
                                                                            <div class="form-group has-error">
                                                                                @else
                                                                                    <div class="form-group">
                                                                                        @endif
                                                                                        <input class="form-control"
                                                                                               name="last_name"
                                                                                               placeholder="Last Name"
                                                                                               value="{{$info['last_name']  or old('last_name') }}">
                                                                                    </div>
                                                                            </div>
                                                                    </div>
                                                            </div>

                                                            @if(!empty($errors->first("address")))
                                                                <div class="form-group has-error">
                                                                    @else
                                                                        <div class="form-group">
                                                                            @endif
                                                                            <label class="required">Address</label>
                                                                            <textarea class="form-control"
                                                                                      name="address">{{$info['address']  or old('address') }}</textarea>
                                                                        </div>

                                                                        <div class="form-group">
                                                                            <label>Contact</label>
                                                                            <div class="row">
                                                                                <div class="col-xs-6">
                                                                                    @if(!empty($errors->first("phone_no")))
                                                                                        <div class="input-group form-group has-error">
                                                                                            @else
                                                                                                <div class="input-group form-group">
                                                                                                    @endif
                                                                                                    <span class="input-group-addon"><i
                                                                                                                class="fa fa-phone"></i></span>
                                                                                                    <input type="text"
                                                                                                           class="form-control"
                                                                                                           placeholder="Phone No."
                                                                                                           name="phone_no"
                                                                                                           value="{{$info['phone_no']  or old('phone_no') }}">
                                                                                                </div>
                                                                                        </div>
                                                                                        <div class="col-xs-6">
                                                                                            @if(!empty($errors->first("mobile_no")))
                                                                                                <div class="input-group form-group has-error">
                                                                                                    @else
                                                                                                        <div class="input-group form-group">
                                                                                                            @endif
                                                                                                            <span class="input-group-addon"><i
                                                                                                                        class="fa fa-mobile-alt"></i></span>
                                                                                                            <input type="text"
                                                                                                                   class="form-control"
                                                                                                                   placeholder="Mobile No."
                                                                                                                   name="mobile_no"
                                                                                                                   value="{{$info['mobile_no']  or old('mobile_no') }}">
                                                                                                        </div>
                                                                                                </div>
                                                                                        </div>
                                                                                </div>

                                                                                @if(!empty($errors->first("email")))
                                                                                    <div class="form-group has-error">
                                                                                        @else
                                                                                            <div class="form-group">
                                                                                                @endif
                                                                                                <label class="required">Email</label>
                                                                                                <div class="input-group">
                                                    <span class="input-group-addon"><i
                                                                class="fa fa-envelope"></i></span>
                                                                                                    <input type="email"
                                                                                                           class="form-control"
                                                                                                           placeholder="Email"
                                                                                                           name="email"
                                                                                                           value="{{$info['email']  or old('email') }}">
                                                                                                </div>
                                                                                            </div>

                                                                                            @if(!empty($errors->first("website")))
                                                                                                <div class="form-group has-error">
                                                                                                    @else
                                                                                                        <div class="form-group">
                                                                                                            @endif
                                                                                                            <label>Website</label>
                                                                                                            <div class="input-group">
                                                                                                                <span class="input-group-addon"><i
                                                                                                                            class="fa fa-globe"></i></span>
                                                                                                                <input type="url"
                                                                                                                       class="form-control"
                                                                                                                       placeholder="Website"
                                                                                                                       name="website"
                                                                                                                       value="{{$info['website']  or old('website') }}">
                                                                                                            </div>
                                                                                                        </div>

                                                                                                        <div class="form-group">
                                                                                                            <label>GPS
                                                                                                                Location</label>
                                                                                                            <div class="input-group">
                                                    <span class="input-group-addon"><i
                                                                class="fa fa-map-marker"></i></span>
                                                                                                                <input type="text"
                                                                                                                       class="form-control"
                                                                                                                       placeholder="Latitude"
                                                                                                                       name="latitude">
                                                                                                                <input type="text"
                                                                                                                       class="form-control"
                                                                                                                       placeholder="Longitude"
                                                                                                                       name="longitude">
                                                                                                            </div>
                                                                                                        </div>

                                                                                                </div>
                                                                                    </div>

                                                                            </div>

                                                                        </div>


        </form>

    </section>

    <div class="modal fade in" id="imagePopup">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h6 class="modal-title">&nbsp;</h6>
                </div>
                <div class="modal-body text-center">
                    <img>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <div class="modal fade" id="modal-edit-password">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Edit Password</h4>
                </div>
                <div class="modal-body">
                    <p>Are you sure to edit password ?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary confirm-edit">Edit</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection
