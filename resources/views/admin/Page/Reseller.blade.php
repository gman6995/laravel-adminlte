@extends(config('theme.back').'Template')
@section('title', 'Reseller')
@section('postscript')
    <script>
        $(function () {
            $(document).on('click', '.delete_btn', function(){
                var id = $(this).data('id');
                $('#modal-delete').modal('show');
                $('.confirm-delete').val(id);
            });

            $('.confirm-delete').click(function(){
                var id = $(this).val();
                $.ajax({
                    type:'GET',
                    url:'/admin/resellers/delete/'+id,
                    success: function(data){
                        if(data== "true"){
                            $('div#delete-alert').html('<br><div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> <i class="icon fa fa-check"></i> Reseller successfully deleted </div>');
                            $("#reseller-row-"+id).remove();
                        }else{
                            $('div#delete-alert').html('<br><div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> <i class="icon fa fa-check"></i> Something went wrong. Please try again later!! </div>');
                        }
                    }
                })
                $('#modal-delete').modal('hide');
            });
        })

    </script>
@endsection
@section('content')
    <section class="content">

        <div id="delete-alert" class="col-xs-12"></div>

        <div class="btn-group pull-right">
            <a href="{{url('/admin/resellers/create')}}" class="btn btn-primary btn-sm">
                <i class="fa fa-plus-circle"></i> New
            </a>
        </div>
        <br><br>

        <div class="box box-white">
            <div class="box-header">
                <h3 class="box-title"><i class="fa fa-store text-muted"></i> All Resellers</h3>
            </div>
            <div class="box-body no-padding">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th width="30"></th>
                        <th width="200"># Name</th>
                        <th width="200">Full name</th>
                        <th>Address</th>
                        <th width="200">Email</th>
                        <th width="150">Actions</th>

                    </tr>
                    </thead>
                    <tbody>
                    @if(isset($data['data']))
                        @foreach($data['data'] as $reseller)
                            <tr id="reseller-row-{{$reseller['id']}}">
                                <td></td>
                                <td>{{$reseller['name']}} <br>
                                    <small class="text-muted">{{$reseller['username']}}</small>
                                </td>
                                <td>{{$reseller['first_name']." ".$reseller['middle_name']." ". $reseller['last_name']}}</td>
                                <td>{{$reseller['address']}}</td>
                                <td>{{$reseller['email']}}</td>
                                <td>
                                    <a class="btn btn-sm btn-info " href="{{ route('reseller.show',['id' => $reseller['id']]) }}" title="View"><i class="fa fa-eye"></i></a>
                                    <a class="btn btn-sm btn-success " href="{{ url('/admin/resellers/' . $reseller['id'] . '/edit') }}" title="Edit"><i class="fa fa-edit"></i></a>

                                    <a class="btn btn-sm btn-danger delete_btn" title="Delete"  data-id="<?php echo $reseller['id'];?>" ><i class="fa fa-minus-circle"></i></a></td>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
            <div class="box-footer">
                <ul class="pagination no-margin pull-right">
                    @if(isset($data['data']))
                        @if($data['prev_page_url'] != null)
                            <li><a href="{{$data['prev_page_url']}}">«</a></li>
                        @endif
                        @for ($i = 1; $i <= $data['last_page']; $i++)
                            <li><a href="/admin/resellers?limit={{$data['per_page']}}&page={{$i}}">{{$i}}</a></li>
                        @endfor
                        @if($data['next_page_url'] != null)
                            <li><a href="{{$data['next_page_url']}}">»</a></li>
                        @endif
                    @endif
                </ul>
            </div>
        </div>
    </section>
    @include(config('theme.back').'Site.deletemodel',['model_title'=> "Delete Reseller"])
@endsection