<!DOCTYPE html>
<html lang="en">
  <head>
    @include(config('theme.back').'Site.header')
    @yield('prescript')
  </head>

  <body class="skin-white sidebar-mini">
    <div class="wrapper">     
        <!-- sidebar -->
        @include(config('theme.back').'Site.sidebar')
        <!-- /sidebar -->

        <!-- top navigation -->
        @include(config('theme.back').'Site.topnav')
        <!-- /top navigation -->

        <!-- page content -->
        <div class="content-wrapper">
          @include(config('theme.back').'Site.breadcrumb')
          @include(config('theme.back').'Site.notification')
          @yield('content')
        </div>
        <!-- /page content -->

        <!-- footer content -->
        @include(config('theme.back').'Site.footer')
        <!-- /footer content -->
        @yield('postscript')
    </div>
  </body>
</html>