<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <i class="fa fa-user-circle text-gray fa-3x"></i>
            </div>
            <div class="pull-left info">
                <p>Some Bahadur</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- search form -->
        <!--<form action="#" method="get" class="sidebar-form">
          <div class="input-group">
            <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                  <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                  </button>
                </span>
          </div>
        </form>-->
        <!-- /.search form -->

        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" id="sidebarMenu">
            <li class="header"></li>
            <li>"
                <a href="{{url('/')}}">
                    <i class="fa fa-tachometer-alt"></i> <span>Dashboard</span>
                </a>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-store"></i>
                    <span>Reseller</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right text-muted"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{url('/admin/resellers')}}"><i class="fa fa-list"></i> All Resellers</a></li>
                    <li><a href="{{url('/admin/resellers/create')}}"><i class="fa fa-plus-circle"></i> New Reseller</a></li>
                </ul>
            </li>

            @if(\Illuminate\Support\Facades\Auth::check())
            <li><a href="{{url('logout')}}" onclick="event.preventDefault();document.getElementById('logout-form').submit();"><i class="fa fa-key"></i> <span>Logout</span></a></li>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: one;">
                    {{ csrf_field() }}
                </form>
            @else
                <li><a href="{{url('login')}}"><i class="fa fa-key"></i> <span>Login</span></a></li>
            @endif
            <li class="header">LABELS</li>
            <li><a href="#"><i class="fa fa-circle text-red"></i> <span>Important</span></a></li>
            <li><a href="#"><i class="fa fa-circle text-yellow"></i> <span>Warning</span></a></li>
            <li><a href="#"><i class="fa fa-circle text-aqua"></i> <span>Information</span></a></li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
<script type="text/javascript">
    $('#sidebarMenu').activeMenu({
        current:"{{Request::url()}}"
    });
</script>