@extends(config('theme.back').'LoginTemplate')
@section('title', 'New User')
@section('postscript')
    <link rel="stylesheet" href="{{asset('plugins/iCheck/square/blue.css')}}">
    <script src="{{asset('plugins/iCheck/icheck.min.js')}}"></script>
    <script type="text/javascript">
        $(function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' // optional
            });
        });
    </script>
@endsection
@section('content')
    <div class="login-box">
        <div class="login-logo">
            <a href="/"><b>Net</b>Tv</a>
        </div>
        <div class="login-box-body">
            <p class="login-box-msg">Sign in to start your session</p>

            @if ($errors->any())
                <div class="alert alert-warning alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    @foreach($errors->getMessages() as $key => $message)
                        {{$errors->first($key)}}<br>
                    @endforeach
                </div>
            @endif

            <form method="POST" action="{{ route('login') }}">
                @csrf
                <div class="form-group has-feedback
                @if ($errors->has('email'))
                        has-error
                        @endif
                        ">
                    <input type="email" name="email" class="form-control" placeholder="Email" value="{{ old('email') }}">
                    <span class="fa fa-user form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback
                        @if ($errors->has('password'))
                        has-error
                        @endif
                        ">
                    <input type="password" name="password" class="form-control" placeholder="Password" value="">
                    <span class="fa fa-key form-control-feedback"></span>
                </div>
                <div class="row">
                    <div class="col-xs-8">
                        <div class="checkbox icheck">
                            <label>
                                <input type="checkbox"
                                       name="remember" {{ old('remember') ? 'checked' : '' }}> {{ __('Remember Me') }}
                            </label>
                        </div>
                    </div><!-- /.col -->
                    <div class="col-xs-4">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
                    </div><!-- /.col -->
                </div>
            </form>

            <a href="{{ route('password.request') }}">I forgot my password</a><br>

        </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->
@endsection
