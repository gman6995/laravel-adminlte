@extends(config('theme.reseller').'Template')
@section('title', 'Dashboard')
@section('postscript')
    <link href="{{asset('plugins/morris/morris.css')}}" type="text/css" rel="stylesheet">
    <script src="{{asset('plugins/chartjs/Chart.js')}}" type="application/javascript"></script>
    <script src="{{asset('plugins/raphael/raphael.js')}}" type="application/javascript"></script>
    <script src="{{asset('plugins/morris/morris.js')}}" type="application/javascript"></script>
    <script type="application/javascript">

        /** LINE CHART **/
        var line = new Morris.Line({
            element: 'salesChart',
            resize: true,
            data: [
                {y: '2011 Q1', item1: 2666},
                {y: '2011 Q2', item1: 2778},
                {y: '2011 Q3', item1: 4912},
                {y: '2011 Q4', item1: 3767},
                {y: '2012 Q1', item1: 6810},
                {y: '2012 Q2', item1: 5670},
                {y: '2012 Q3', item1: 4820},
                {y: '2012 Q4', item1: 15073},
                {y: '2013 Q1', item1: 10687},
                {y: '2013 Q2', item1: 8432}
            ],
            xkey: 'y',
            ykeys: ['item1'],
            labels: ['Item 1'],
            lineColors: ['#00c0ef'],
            lineWidth: 1,
            hideHover: 'auto',
            gridTextColor: '#999',
            gridStrokeWidth: 0.4,
            pointSize: 4,
            pointStrokeColors: ['#efefef'],
            gridLineColor: '#efefef',
            gridTextFamily: 'Open Sans',
            gridTextSize: 10
        });
    </script>
@endsection
@section('content')
    <section class="content">
        <div class="row">
            <div class="col-xs-12">

                <div class="row">
                    <div class="col-12 col-sm-3">
                        <div class="info-box">
                            <span class="info-box-icon bg-green"><i class="fa fa-user-check"></i></span>
                            <div class="info-box-content">
                                <span class="info-box-text">Active Users</span>
                                <span class="info-box-number">9000</span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                    </div>

                    <div class="col-12 col-sm-3">
                        <div class="info-box">
                            <span class="info-box-icon bg-aqua"><i class="fa fa-user-plus"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">New Customers in Last 30 days</span>
                                <span class="info-box-number">126</span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                    </div>

                    <div class="col-12 col-sm-3">
                        <div class="info-box">
                            <span class="info-box-icon bg-teal"><i class="fa fa-archive"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">Active Packages</span>
                                <span class="info-box-number">9</span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                    </div>

                    <div class="col-12 col-sm-3">
                        <div class="info-box">
                            <span class="info-box-icon bg-teal"><i class="far fa-hdd"></i></span>

                            <div class="info-box-content">
                                <span class="info-box-text">STB in stock</span>
                                <span class="info-box-number">900</span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                    </div>
                </div>


                <div class="row">

                    <div class="col-xs-12 col-sm-6 col-md-3">
                        <div class="box box-white">
                            <div class="box-header">
                                <h3 class="box-title"><i class="fab fa-mixcloud text-gray"></i> Top Channels</h3>
                            </div>
                            <div class="box-body no-padding">
                                <ul class="nav nav-stacked">
                                    <li><a href="#">Star Sports HD <span
                                                    class="pull-right fab fa-google text-lime"></span></a></li>
                                    <li><a href="#">Colors HD <span
                                                    class="pull-right fa fa-globe text-green"></span></a></li>
                                    <li><a href="#">Star Plus <span class="pull-right fa fa-star text-blue"></span></a>
                                    </li>
                                    <li><a href="#">Zee TV HD <span
                                                    class="pull-right fab fa-telegram-plane text-aqua"></span></a></li>
                                    <li><a href="#">AP1 HD <span class="pull-right fab fa-apple text-teal"></span></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-3">
                        <div class="box box-white">
                            <div class="box-header">
                                <h3 class="box-title"><i class="fa fa-video text-gray"></i> Top Shows</h3>
                            </div>
                            <div class="box-body no-padding">
                                <ul class="nav nav-stacked">
                                    <li><a href="#">Show 1 <span class="pull-right badge bg-blue">30081</span></a></li>
                                    <li><a href="#">Show 2 <span class="pull-right badge bg-aqua">25045</span></a></li>
                                    <li><a href="#">Show 3 <span class="pull-right badge bg-green">21224</span></a></li>
                                    <li><a href="#">Show 4 <span class="pull-right badge bg-red">18429</span></a></li>
                                    <li><a href="#">Show 5 <span class="pull-right badge bg-red">18120</span></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-3">
                        <div class="box box-white">
                            <div class="box-header">
                                <h3 class="box-title"><i class="fa fa-film text-gray"></i> Top Movies</h3>
                            </div>
                            <div class="box-body no-padding">
                                <ul class="nav nav-stacked">
                                    <li><a href="#">Deadpool <span class="pull-right badge bg-blue">5031</span></a></li>
                                    <li><a href="#">Bahubali <span class="pull-right badge bg-aqua">5009</span></a></li>
                                    <li><a href="#">Ice Age 2 <span class="pull-right badge bg-green">3412</span></a>
                                    </li>
                                    <li><a href="#">Avengers <span class="pull-right badge bg-red">2842</span></a></li>
                                    <li><a href="#">Three Idiots <span class="pull-right badge bg-red">2842</span></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-3">
                        <div class="box box-white">
                            <div class="box-header">
                                <h3 class="box-title"><i class="fa fa-archive text-gray"></i> Popular Package</h3>
                            </div>
                            <div class="box-body no-padding">
                                <ul class="nav nav-stacked">
                                    <li><a href="#">Sports HD <span class="pull-right badge bg-blue">1029</span></a>
                                    </li>
                                    <li><a href="#">Entertainment<span class="pull-right badge bg-aqua">998</span></a>
                                    </li>
                                    <li><a href="#">Movies <span class="pull-right badge bg-green">600</span></a>
                                    </li>
                                    <li><a href="#">Sports <span class="pull-right badge bg-red">563</span></a></li>
                                    <li><a href="#">News<span class="pull-right badge bg-red">345</span></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="col-xs-12 col-sm-9">
                <div class="box box-white">
                    <div class="box-header">
                        <h3 class="box-title"><i class="fa fa-chart-line"></i> Sales Chart</h3>
                    </div>
                    <div class="box-body bg-white">
                        <div id="salesChart" class="chart"></div>
                    </div>
                </div>


            </div>

            <div class="col-xs-12 col-sm-3">

                <div class="box box-white">
                    <div class="box-header with-border">
                        <h4 class="box-title">Recent Updates</h4>
                    </div>
                    <div class="box-body" style="height: 360px;overflow-y: auto;">
                        <ul class="products-list product-list-in-box">
                            <li class="item with-border"><span class="fa fa-tv text-warning"></span> <a
                                        href="">viewer101</a> subscription expired
                            </li>
                            <li class="item with-border"><span class="fa fa-archive text-green"></span> <a href="">SportsHD</a>
                                Package created
                            </li>
                            <li class="item with-border"><span class="fa fa-tv text-green"></span> <a
                                        href="">Discovery</a> Channel enabled
                            </li>

                            <li class="item with-border"><span class="fa fa-user-minus text-warning"></span> Subscriber
                                <a
                                        href="">viewer011</a> disabled
                            </li>
                            <li class="item with-border"><span class="fa fa-user-plus text-danger"></span> Subscriber <a
                                        href="">viewer001</a> deleted
                            </li>
                            <li class="item with-border"><span class="fa fa-user-minus text-warning"></span> Subscriber
                                <a
                                        href="">viewer001</a> disabled
                            </li>
                            <li class="item with-border"><span class="fa fa-user-plus text-aqua"></span> Subscriber <a
                                        href="">viewer001</a> added
                            </li>
                            <li class="item with-border"><span class="fa fa-user-plus text-aqua"></span> Subscriber <a
                                        href="">viewer001</a> added
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection