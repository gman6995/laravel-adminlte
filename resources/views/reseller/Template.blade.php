<!DOCTYPE html>
<html lang="en">
  <head>
    @include(config('theme.reseller').'Site.header')
    @yield('prescript')
  </head>

  <body class="skin-blue-light sidebar-mini">
    <div class="wrapper">
        <!-- sidebar -->
        @include(config('theme.reseller').'Site.sidebar')
        <!-- /sidebar -->

        <!-- top navigation -->
        @include(config('theme.reseller').'Site.topnav')
        <!-- /top navigation -->

        <!-- page content -->
        <div class="content-wrapper">
          @include(config('theme.reseller').'Site.breadcrumb')
          @include(config('theme.reseller').'Site.notification')
          @yield('content')
        </div>
        <!-- /page content -->

        <!-- footer content -->
        @include(config('theme.reseller').'Site.footer')
        <!-- /footer content -->
        @yield('postscript')
    </div>
  </body>
</html>