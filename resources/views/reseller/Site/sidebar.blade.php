<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <i class="fa fa-user-circle fa-3x"></i>
            </div>
            <div class="pull-left info">
                <p>Reseller Name</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>


        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header"></li>
            <li>
                <a href="{{url('/')}}">
                    <i class="fa fa-tachometer-alt"></i> <span>Dashboard</span>
                </a>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-users"></i>
                    <span>Users</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right text-muted"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{url('users')}}"><i class="fa fa-list"></i> All Users</a></li>
                    <li><a href="{{url('user/new')}}"><i class="fa fa-user-plus"></i> New User</a></li>
                </ul>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-archive"></i>
                    <span>Package</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right text-muted"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{url('package')}}"><i class="fa fa-list"></i> My Packages</a></li>
                    <li><a href="{{url('package/new')}}"><i class="fa fa-plus-circle"></i> New Package</a></li>
                </ul>
            </li>

            <li><a href="{{url('stb/inventory')}}"><i class="fa fa-warehouse"></i> <span>STB Inventory</span></a></li>

            <li><a href="#"><i class="fa fa-clipboard-list"></i> <span>Reports</span></a></li>

            <li><a href="{{url('login')}}"><i class="fa fa-key"></i> <span>Login</span></a></li>

            <li class="header">LABELS</li>
            <li><a href="#"><i class="fa fa-circle text-red"></i> <span>Important</span></a></li>
            <li><a href="#"><i class="fa fa-circle text-yellow"></i> <span>Warning</span></a></li>
            <li><a href="#"><i class="fa fa-circle text-aqua"></i> <span>Information</span></a></li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>