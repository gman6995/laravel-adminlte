@if ($errors->any())
<div class="col-xs-12">
	<br>
	<div class="alert alert-warning alert-dismissible">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		<i class="icon fa fa-warning"></i>
		@foreach($errors->getMessages() as $key => $message)
	    	{{$errors->first($key)}}
		@endforeach
	</div>	
</div>
@endif

@if (session('success'))
<div class="col-xs-12">
	<br>
	<div class="alert alert-success alert-dismissible">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		<i class="icon fa fa-check"></i> {{session('success')}}
	</div>	
</div>
@endif

@if (session('error'))
<div class="col-xs-12">
	<br>
	<div class="alert alert-warning alert-dismissible">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		<i class="icon fa fa-warning"></i> {{session('error')}}
	</div>	
</div>
@endif