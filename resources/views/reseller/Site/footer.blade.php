<footer class="main-footer hidden">
<div class="pull-right hidden-xs">
  <b>Version</b> 2.3.8
</div>
<strong>Copyright &copy; {{date('Y')}} <a href="http://sujanbyanjankar.com.np" target="_blank">Sujan</a>.</strong> 
All rights reserved.
</footer>
<script src="{{asset('js/all.js')}}"></script>

@if ($errors->any())
<script type="text/javascript">
	gErrKey = $("#{{implode(',#',array_keys($errors->getMessages()))}}");
	gErrKey.parent().addClass('has-error');
</script>
@endif