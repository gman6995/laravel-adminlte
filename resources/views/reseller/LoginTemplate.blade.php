<!DOCTYPE html>
<html lang="en">
<head>
    @include(config('theme.back').'Site.header')
    @yield('prescript')
</head>

<body class="login-page">

@yield('content')

@yield('postscript')
</div>
</body>
</html>