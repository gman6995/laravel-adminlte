var selectListSorting = false;
(function () {
    $('#select-list #addToList').click(function (e) {
        var selectedOpts = $('#fromList option:selected');
        if (selectedOpts.length == 0) {
            alert("Select item to move.");
            e.preventDefault();
        }

        $('#toList').append($(selectedOpts).clone().addClass('bg-gray'));
        $(selectedOpts).remove();
        e.preventDefault();

        sortOptionList('#toList');

        setTimeout(function () {
            $('#toList option').removeClass("bg-gray");
        }, 800);
    });


    $('#fromList').on('dblclick', 'option', function (e) {
        $('#toList').append($(this).clone().addClass(
            'bg-gray'));
        $(this).remove();
        e.preventDefault();

        sortOptionList('#toList');;

        setTimeout(function () {
            $('#toList option').removeClass("bg-gray");
        }, 800);
    });

    $('#select-list #addAllToList').click(function (e) {
        var selectedOpts = $('#fromList option');
        if (selectedOpts.length == 0) {
            alert("Select item to move.");
            e.preventDefault();
        }

        $('#toList').append($(selectedOpts).clone());
        $(selectedOpts).remove();
        e.preventDefault();
    });

    $('#select-list #removeFromList').click(function (e) {
        var selectedOpts = $('#toList option:selected');
        if (selectedOpts.length == 0) {
            alert("Select item to move.");
            e.preventDefault();
        }

        $('#fromList').append($(selectedOpts).clone().addClass('bg-gray'));
        $(selectedOpts).remove();
        e.preventDefault();

        sortOptionList('#fromList');

        setTimeout(function () {
            $('#fromList option').removeClass("bg-gray");
        }, 800);
    });

    $('#toList').on('dblclick', 'option', function (e) {
        $('#fromList').append($(this).clone().addClass('bg-gray'));
        $(this).remove();
        e.preventDefault();

        sortOptionList('#fromList');

        setTimeout(function () {
            $('#fromList option').removeClass("bg-gray");
        }, 800);
    });


    $('#select-list #removeAllFromList').click(function (e) {
        var selectedOpts = $('#toList option');
        if (selectedOpts.length == 0) {
            alert("Select item to move.");
            e.preventDefault();
        }

        $('#fromList').append($(selectedOpts).clone());
        $(selectedOpts).remove();
        e.preventDefault();
    });

}(jQuery));

function sortOptionList(el){
    if(!selectListSorting) return;
    var list = $(el);
    list.html(list.find('option').sort(function(x, y) {
        return $(x).text().toLowerCase() > $(y).text().toLowerCase() ? 1 : -1;
    }));
}


