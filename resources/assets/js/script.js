function readURL(input,el) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            $('#'+el).attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}


function previewSnap(input){
    var elo = $(input);
    var preview = $(elo.data('preview'));

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            preview.attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

function searchOptions(listID,term) {
    var option, item, selectList, filter;
    selectList = document.getElementById(listID);
    option = selectList.getElementsByTagName('option');
    filter = term.value;
    for (i = 0; i < option.length; i++) {
        item = option[i];
        if (item) {
            if (item.innerHTML.toUpperCase().indexOf(filter) > -1) {
                option[i].style.display = "";
            } else {
                option[i].style.display = "none";
            }
        }
    }
}




/*

var draggables = $('#select-list .btn').each(function () {
    $(this).attr('draggable',true);
})


var draggablesPos = [];
draggables.each(function (index,value) {
    console.log($(this).position());
    draggablesPos[index] = $(this).position();
})
$('#select-list').on('dragstart','.btn',function (event) {
    var elbound = $(this).position();
    y = elbound.top;
    x = elbound.left;
    console.log(x+' '+y);
    //console.log($(this).next().position());

    p = $(this).parent();


    //console.log(draggablesPos);
})
var stop = {'top':0,'left':0};
$('#select-list').on('dragleave','.btn',function (event) {

    var elbound = $(event.target).position();
    console.log(elbound);
    $.each(draggablesPos,function (index,value) {
        if(elbound.top > value.top) {
            stop.top = value.top;

            console.log('leave');
            console.log(index);
        }

    })



})

$('#select-list').on('dragend','.btn',function (event) {
    console.log('stopped');
    console.log(stop);
});
*/