/*
 Info:
  Plugin: selectList
  Version: 1.1
  Author: Sujan Byanjankar
  E-mail: byanjankarsujan@gmail.com
*/
(function ($) {

    $.fn.selectList = function (options) {

        var settings = $.extend({
            from: '#fromList',
            to: '#toList',
            sort: true,
            adder: '#add',
            remover: '#remove',
            mover: '#move',
            cleaner: '#clean',
            notifier: 'select-highlight',
            notifytimer: 800, // miliseconds
            fromSearch:'#fromListSearch',
            toSearch:'#toListSearch'
        }, options);


        var from = $(this).find(settings.from);
        var to = $(this).find(settings.to);
        var adder = $(this).find(settings.adder);
        var remover = $(this).find(settings.remover);
        var mover = $(this).find(settings.mover);
        var cleaner = $(this).find(settings.cleaner);
        var fromSearch = $(this).find(settings.fromSearch);
        var toSearch = $(this).find(settings.toSearch);

        // on add button click
        adder.click(function (e) {
            var selectedOpts = from.find('option:selected');
            if (selectedOpts.length == 0) {
                alert("Select item to move.");
                e.preventDefault();
            }

            to.append($(selectedOpts).clone().addClass(settings.notifier));
            $(selectedOpts).remove();
            e.preventDefault();

            methods.sortOptionList(settings.to);
            methods.removeNotification(settings.to);
            methods.setSelected();
        });

        // on remove button click
        remover.click(function (e) {
            var selectedOpts = to.find('option:selected');
            if (selectedOpts.length == 0) {
                alert("Select item to move.");
                e.preventDefault();
            }

            from.append($(selectedOpts).clone().addClass(settings.notifier));
            $(selectedOpts).remove();
            e.preventDefault();

            methods.sortOptionList(settings.from);
            methods.removeNotification(settings.from);
            methods.setSelected();
        });


        // on double click on available option
        from.on('dblclick', 'option', function (e) {
            to.append($(this).clone().addClass(settings.notifier));
            $(this).remove();
            e.preventDefault();

            methods.sortOptionList(settings.to);
            methods.removeNotification(settings.to);
            methods.setSelected();
        });

        // on double click on selected option
        to.on('dblclick', 'option', function (e) {
            from.append($(this).clone().addClass(settings.notifier));
            $(this).remove();
            e.preventDefault();

            methods.sortOptionList(settings.from);
            methods.removeNotification(settings.from);
            methods.setSelected();
        });

        // on add all button click
        mover.click(function (e) {
            var selectedOpts = from.find(' option');
            if (selectedOpts.length == 0) {
                alert("Select item to move.");
                e.preventDefault();
            }

            to.append($(selectedOpts).clone());
            $(selectedOpts).remove();
            e.preventDefault();
            methods.setSelected();
        });

        // on remove all button click
        cleaner.click(function (e) {
            var selectedOpts = to.find(' option');
            if (selectedOpts.length == 0) {
                alert("Select item to move.");
                e.preventDefault();
            }

            from.append($(selectedOpts).clone());
            $(selectedOpts).remove();
            e.preventDefault();
            methods.setSelected();
        });

        // on search on From List
        fromSearch.keyup(function(){
            methods.searchInList(settings.from,this);
        })

        // on search on To List
        toSearch.keyup(function(){
            methods.searchInList(settings.to,this);
        })

        var methods = {
            'sortOptionList': function (el) {
                if (!settings.sort) return;
                var list = $(el);
                list.html(list.find('option').sort(function (x, y) {
                    return $(x).text().toLowerCase() > $(y).text().toLowerCase() ? 1 : -1;
                }));
            },
            'removeNotification': function (el) {
                setTimeout(function () {
                    $(el).find('option').removeClass(settings.notifier);
                }, settings.notifytimer);
            },
            'searchInList': function (el, term) {
                var option, item, selectList, filter;
                selectList = $(el);
                option = selectList.find('option');
                filter = term.value.toUpperCase();
                for (i = 0; i < option.length; i++) {
                    item = option[i];
                    if (item) {
                        if (item.innerHTML.toUpperCase().indexOf(filter) > -1) {
                            option[i].style.display = "";
                        } else {
                            option[i].style.display = "none";
                        }
                    }
                }
            },
            'setSelected':function(){
                to.find('option').prop('selected',true);
            }
        }
    };

}(jQuery));