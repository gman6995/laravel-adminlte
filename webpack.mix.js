let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
		.scripts('resources/assets/js/adminlte.js','public/js/all.js')
   .sass('resources/assets/sass/app.scss', 'public/css')
	.copy('resources/assets/css/custom.skin.css','public/css/custom.skin.css')
    .copy('resources/assets/css/reseller.skin.css','public/css/reseller.skin.css')
   .copy('resources/assets/plugins','public/plugins')
	.copy('resources/assets/js/script.js','public/js/script.js')
    //.copy('resources/assets/js/select-list.js','public/js/select-list.js')
	.copy('resources/assets/js/selectList.lib.js','public/js/select-list.js');