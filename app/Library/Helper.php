<?php
/**
 * Created by PhpStorm.
 * User: sujan
 * Date: 5/22/18
 * Time: 5:02 PM
 */

namespace App\Library;


class Helper
{

    public static function generateRandomString(int $length = null){
        $length = ($length == null)? 10 : $length;
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ- ';
        $charLen = strlen($characters);
        $str = '';
        for ($i = 0; $i < $length; $i++) {
            $str .= $characters[rand(0, $charLen - 1)];
        }
        return $str;
    }
}