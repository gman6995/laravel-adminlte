<?php
/**
 * Created by PhpStorm.
 * @author Kabina Suwal <kabina.suwal92@gmail.com>
 * Date: 21-May-18
 * Time: 12:29 PM
 */

namespace App\Http\Facades;


use Illuminate\Support\Facades\Facade;

/**
 * Class RemoteCallFacade
 * @package App\Http\Facades
 */
class RemoteCallFacade extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor()
{
  return "remoteCall";
}
}