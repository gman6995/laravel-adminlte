<?php

namespace App\Http\Controllers;

use App\Http\Facades\RemoteCallFacade;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

class ResellerController extends BaseController
{
    private $guzzle;

    /**
     * ResellerController constructor.
     */
    public function __construct(Client $guzzle)
    {
        $this->guzzle = $guzzle;
    }

    public function index(Request $request)
    {
        $page = $request->get("page", 1);
        $limit = $request->get("limit", 10);
        $base_url = config("config.channel_base_url");
        $url = $base_url . "/admin/resellers?limit=$limit&page=$page";
        $data = RemoteCallFacade::getSpecific($url);
        if ($data['status'] != 200) {
            return view(config('theme.back') . 'Page.Reseller', ['data' => []])->withErrors($data['message'] = ['message' => "Something went wrong. Please try again later"]);
        }
        $data = $data['message'];
        return view(config('theme.back') . 'Page.Reseller', ['data' => $data]);

    }

    public function create()
    {
        $config = config("config.channel_base_url");
        return view(config('theme.back') . 'Page.Reseller-new', compact('config'));
    }

    public function store(Request $request)
    {
        try {
            $create = $request->except(['logo','password']);
            if($request->filled('password')){
                $create['password']= $request['password'];
            }
            $file = $request->file('logo');
            if ($request->hasFile('logo')) {
                $imagePath = addslashes($file->getPathName());
                $encoded_image = "data:image;base64," . base64_encode(file_get_contents($imagePath));
                $validator = \Validator::make($request->all(), [
                    "logo" => "required|dimensions:width=1024,height=512"
                ]);

                if ($validator->fails()) {
                    return redirect()->route('reseller.create')->withErrors($validator)->withInput($request->all());

                }
                $request['logo_preview'] = $encoded_image;
                $create['logo'] = $encoded_image;
            }else {
                if ($request->filled('logo_preview')) {
                    $create['logo'] = $request['logo_preview'];
                }

            }
            $attachment = [];
            if ($request->has('selectedImageNames')) {
                if (count($request['selectedImageNames']) > 0 && count($request['selectedImages'])) {
                    foreach ($request['selectedImages'] as $key => $image) {
                        $attachment[$key]['name'] = $request['selectedImageNames'][$key];
                        $attachment[$key]['image'] = $image;

                    }
                }
            }

            if (!empty($attachment)) {
                $create['details'] = $attachment;
            }
            $url = config("config.channel_base_url") . "/admin/resellers";
            $data = RemoteCallFacade::storeWithoutOauth($url, $create);
            if ($data['status'] != 200) {
                if ($data['status'] == 422) {
//                return $data['message']['message'];
                    return redirect()->route('reseller.create')->withErrors($data['message']['message'])->withInput($request->all());
                }
                throw new \Exception();
            }
            return redirect()->route('reseller.index')->with(["success" => "Reseller Created Successfully."]);
        } catch (\Exception $ex) {
            return redirect()->route('reseller.create')->withErrors($data['message'] = ['message' => "Something went wrong. Please try again later"]);
        }
    }

    public function edit($id)
    {
        $url = config('config.channel_base_url') . "/admin/resellers/$id";
        $data = RemoteCallFacade::getSpecificWithoutToken($url);
        try {
            if ($data["status"] != 200) {
                throw new \Exception();
            }
            $action = route('reseller.update', $id);
            $method = "PUT";
            $submit_text = "Update";
            $info = $data['message'];
            $config = config("config.channel_base_url");
            $info['logo'] = isset($info['logo'])?$info['logo']:" ";
            $info['logo_preview'] = $info['logo'];
            $info['password'] = "";

//            if (isset($data['message']['logo'])) {
//                $info['logo_preview'] = $data['message']['logo'];
//            }

            return view(config('theme.back') . 'Page.Reseller-edit', compact('info', 'action', 'method', 'submit_text', 'config'));
        } catch (\Exception $ex) {
            return redirect()->route('reseller.index')->withErrors(["Something went wrong. Please try again later!!!"]);
        }
    }

    public function show($id)
    {
        $url = config('config.channel_base_url') . "/admin/resellers/$id";
        $data = RemoteCallFacade::getSpecificWithoutToken($url);
        try {
            if ($data["status"] != 200) {
                throw new \Exception();
            }
            $action = route('reseller.show', $id);
            $method = "PUT";
            $submit_text = "Update";
            $info = $data['message'];
            $config = config("config.channel_base_url");
            $info['logo'] = isset($info['logo'])?$info['logo']:" ";
            $info['logo_preview'] = $info['logo'];


            return view(config('theme.back') . 'Page.Reseller-show', compact('id', 'info', 'action', 'method', 'submit_text', 'config'));
        } catch (\Exception $ex) {
            return redirect()->route('category.index')->withErrors(["Something went wrong. Please try again later!!!"]);
        }
    }

    public function update($id, Request $request)
    {
            $create = $request->except(['logo','password']);
            if($request->has('password') && $request->filled('password')){
                $create['password'] = $request['password'];
            }
            $file = $request->file('logo');
            if ($request->hasFile('logo')) {
                $imagePath = addslashes($file->getPathName());
                $encoded_image = "data:image;base64," . base64_encode(file_get_contents($imagePath));

                $request['logo_preview'] = $encoded_image;
                $validator = \Validator::make($request->all(), [
                    "logo" => "required|dimensions:width=1024,height=512"
                ]);

                if ($validator->fails()) {
                    return redirect()->route('reseller.edit', $id)->withErrors($validator)->withInput($request->all());

                }
                $request['logo_preview'] = $encoded_image;
                $create['logo'] = $encoded_image;
            }else{
                if($request->filled('logo_preview'))
                 $create['logo'] = $request['logo_preview'];
            }
            $attachment = [];
            if ($request->has('selectedImageNames')) {
                if (count($request['selectedImageNames']) > 0 && count($request['selectedImages'])) {
                    foreach ($request['selectedImages'] as $key => $image) {
                        $attachment[$key]['name'] = $request['selectedImageNames'][$key];
                        $attachment[$key]['image'] = $image;
                    }
                }
            }

            if (!empty($attachment)) {
                $create['details'] = $attachment;
            }
            try {
                $url = config("config.channel_base_url") . "/admin/resellers/$id";
                $data = RemoteCallFacade::storeWithoutOauth($url, $create);
                if ($data['status'] != 200) {
                    if ($data['status'] == 422) {
                        return redirect()->route('reseller.edit', $id)->withErrors($data['message']['message'])->withInput($request->all());
                    }
//            dd($data['message']);
                    throw new \Exception();
                }
            }catch (\Exception $e){
                return redirect()->route('reseller.edit', $id)->withErrors(["Something went wrong. Please try again later!!!"])->withInput($request->all());
            }
            return redirect()->route('reseller.index', $id)->with(["success" => "Reseller Updated Successfully."]);
        try {
        } catch (\Exception $ex) {
            return redirect()->route('reseller.index')->withErrors(["Something went wrong. Please try again later!!!"])->withInput($request->all());
        }
    }

    public function delete($id)
    {
        try {
            $url = config('config.channel_base_url') . "/admin/resellers/$id";

            $data = RemoteCallFacade::deleteSpecific($url);

            if ($data["status"] != 200) {
                return "false";
            } else {
                return "true";
            }

        } catch (\Exception $ex) {
            return "false";
        }

    }
}


// Testing Codes
//                        $req[]=[
//                            'name'     => 'logo',
//                            'filename' => $request['logo']->getClientOriginalName(),
//                            'Mime-Type'=> $request['logo']->getmimeType(),
//                            'contents' => fopen( $request['logo']->getPathname(), 'r' ),
//                        ];