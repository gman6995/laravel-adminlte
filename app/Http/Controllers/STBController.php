<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use App\Http\Facades\RemoteCallFacade;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class STBController extends Controller
{
    private $guzzle;

    public function __construct(Client $guzzle)
    {
        $this->guzzle = $guzzle;
    }

    public function index(Request $request)
    {
        $page = $request->get("page", 1);
        $limit = $request->get("limit", 10);
        $url = "http://reseller.nettv.com.np/stbs?limit=$limit&page=$page";
        $data = RemoteCallFacade::getSpecificWithoutToken($url);
        if ($data['status'] != 200) {
            return view(config('theme.back') . 'Page.STB-inventory', ['data' => []])->withErrors($data['message'] = ['message' => "Something went wrong. Please try again later"]);
        }
        $data = $data['message'];
        return view(config('theme.back') . 'Page.STB-inventory', ['data' => $data]);
    }

    public function fileUpload(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'import_file' => 'required|file',
        ]);

        if ($validator->fails()) {
            return redirect()->route('stb.new')->withErrors($validator)->withInput($request->all());
        }
        $path = $request->file('import_file')->getRealPath();
        $file = fopen($path, "r");
        $url = "http://reseller.nettv.com.np/stb-import/check";
        $result = $this->guzzle->post($url, [
            'headers' => [
                'Accept' => 'application/json'
            ],
            'multipart' => [
                [
                    'name' => 'import_file',
                    'contents' => $file
                ]
            ],
        ]);
        $response=json_decode((string) $result->getBody(), true);

        return ["message"=>$response,"status"=>$result->getStatusCode()];
        dd($result);

    }

    public function insert(Request $request)
    {
        $data = new \stdClass();
        if ($request->file('csvfile')) {
            $data->import_file = $request->file('csvfile');
            dd($data);
            $url = "reseller.nettv.com.np/stb-import";
            $result = $this->guzzle->post($url, [
                'headers' => [
                    'Content-Type' => 'application/x-www-form-urlencoded'
                ],
                'form_params' => $data,
                'multipart' => [
                    [
                        'name' => 'FileContents',
                        'contents' => file_get_contents($path . $name),
                        'filename' => $name
                    ],
                    [
                        'name' => 'FileInfo',
                        'contents' => json_encode($fileinfo)
                    ]
                ]
            ]);
            var_dump($result);
            exit;
            $post = RemoteCallFacade::storeWithoutOauth($url, $data);
            if ($post["status"] !== 200) {
                return Redirect::back()->withErrors($post["message"]["message"])->withInput($request->all());
            }
        }
        $mac = $request->mac;
        $serial = $request->serial;
        $vendor = $request->vendor;
        $model = $request->model;
        $data->mac = $mac;
        $data->serial = $serial;
        $data->vendor = $vendor;
        $data->model = $model;
        $url = "reseller.nettv.com.np/stbs";
        $post = RemoteCallFacade::storeWithoutOauth($url, $data);

        if ($post["status"] !== 200) {
            return Redirect::back()->withErrors($post["message"]["message"])->withInput($request->all());
        }
    }

    public function assign()
    {
        $data = new Collection();
        $resellerurl = "http://new.nettv.com.np/public/channelAndReseller";
        $reseller = RemoteCallFacade::getSpecific($resellerurl);
        $data['reseller'] = $reseller['message']['resellers']['data'];
        $stburl = "http://reseller.nettv.com.np/unused/stbs";
        $stbs = RemoteCallFacade::getSpecific($stburl);
        $data['stbs'] = count($stbs["message"]["data"]);
//        dd($data);exit;
        return view(config('theme.back') . 'Page.STB-assign', ['data' => $data]);
    }
}