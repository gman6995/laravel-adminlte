<?php

namespace App\Http\Controllers;

use App\Http\Facades\RemoteCallFacade;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    private $guzzle;

    /**
     * CategoryController constructor.
     */
    public function __construct(Client $guzzle)
    {
        $this->guzzle = $guzzle;
    }

    public function index(Request $request)
    {
        $page = $request->get("page", 1);
        $limit = $request->get("limit", 10);
        $base_url = config("config.channel_base_url");
        $url = $base_url . "/admin/categories?limit=$limit&page=$page";
        $data = RemoteCallFacade::getSpecific($url);
        if ($data['status'] != 200) {
            return view(config('theme.back') . 'Page.Channel-category', ['data' => []])->withErrors($data['message'] = ['message' => "Something went wrong. Please try again later"]);
        }
        $data = $data['message'];
        return view(config('theme.back') . 'Page.Channel-category', ['data' => $data]);

    }

    public function create()
    {
        $url = config("config.channel_base_url") . "/public/channelAndReseller";
        $data = RemoteCallFacade::getSpecific($url);
        if ($data['status'] != 200) {
            return view(config('theme.back') . 'Page.Channel-category-new')->withErrors($data['message'] = ['message' => "Something went wrong. Please try again later"]);
        }
        return view(config('theme.back') . 'Page.Channel-category-new', ['data' => $data['message']]);
    }

    public function store(Request $request)
    {
//        return $request->all();
        $create = $request->except(['logo', 'channels']);
        $returnData = $request->except(['logo']);
        $file = $request->file('logo');
        if ($request->hasFile('logo')) {
            $imagePath = addslashes($file->getPathName());
            $encoded_image = "data:image;base64,".base64_encode(file_get_contents($imagePath));
            $validator =\Validator::make($request->all(), [
                "logo" => "required|dimensions:width=1024,height=1024",
            ]);

            if ($validator->fails()) {
                return redirect()->route('category.create')->withErrors($validator)->withInput($request->all());

            }
            $request['logo_preview'] = $encoded_image;
            $create['logo'] = $encoded_image;
        }else {
            if ($request->filled('logo_preview')) {
                $create['logo'] = $request['logo_preview'];
            }

        }
        if ($request->has('channels')) {
            foreach ($request['channels'] as $key => $channel) {
                $create['channels'][$key]['channel_id'] = $channel;
                $create['channels'][$key]['priority'] = $key + 1;
            }

        }
//        return $create;
        $url = config("config.channel_base_url") . "/admin/categories";
        $data = RemoteCallFacade::storeWithoutOauth($url, $create);
        if ($data['status'] != 200) {
            if ($data['status'] == 422) {
                // return $data['message']['message'];
                return redirect()->route('category.create')->withErrors($data['message']['message'])->withInput($request->all());
            }
            return redirect()->route('category.create')->withInput($request->all())->withErrors($data['message'] = ['message' => "Something went wrong. Please try again later"]);
        }
        return redirect()->route('category.index')->with(["success" => "Category Created Successfully."]);

    }

    public function edit($id)
    {
        $url = config('config.channel_base_url') . "/admin/categories/$id";
        $data = RemoteCallFacade::getSpecificWithoutToken($url);
        try {
            if ($data["status"] != 200) {
                throw new \Exception();
            }
//        return $data;
            $action = route('category.update', $id);
            $method = "PUT";
            $submit_text = "Update";
            $info = $data['message'];
            $config = config("config.channel_base_url");
            $info['logo'] = isset($info['logo'])?$info['logo']:" ";
            $info['logo_preview'] = $info['logo'];

            $url = config("config.channel_base_url") . "/public/channelAndReseller";
            $data = RemoteCallFacade::getSpecific($url);
            if ($data['status'] != 200) {
                throw new \Exception();
            }
            $data = $data['message'];
//        return $info;
            return view(config('theme.back') . 'Page.Channel-category-edit', compact('info', 'action', 'method', 'submit_text', 'config', 'data'));
        } catch (\Exception $ex) {
            return redirect()->route('category.index')->withErrors(["Something went wrong. Please try again later!!!"]);
        }
    }


    public function show($id)
    {
        $url = config('config.channel_base_url') . "/admin/categories/$id";
        $data = RemoteCallFacade::getSpecificWithoutToken($url);
        try {
            if ($data["status"] != 200) {
                throw new \Exception();
            }
//        return $data;
            $action = route('category.update', $id);
            $method = "PUT";
            $submit_text = "Update";
            $info = $data['message'];
            $config = config("config.channel_base_url");
            $info = array_except($info, ['logo']);
            if (isset($data['message']['logo'])) {
                $info['logo_preview'] = $data['message']['logo'];
            }

            $url = config("config.channel_base_url") . "/public/channelAndReseller";
            $data = RemoteCallFacade::getSpecific($url);
            if ($data['status'] != 200) {
                throw new \Exception();
            }
            $data = $data['message'];
//        return $info;
            return view(config('theme.back') . 'Page.Channel-category-show', compact('id', 'info', 'action', 'method', 'submit_text', 'config', 'data'));
        } catch (\Exception $ex) {
            return redirect()->route('category.index')->withErrors(["Something went wrong. Please try again later!!!"]);
        }
    }

    public function update($id, Request $request)
    {
        $create = $request->except(['logo', 'channels']);
        $returnData = $request->except(['logo']);
        $file = $request->file('logo');
        if ($request->hasFile('logo')) {
            $imagePath = addslashes($file->getPathName());
            $encoded_image = "data:image;base64,".base64_encode(file_get_contents($imagePath));
            $validator =\Validator::make($request->all(), [
                "logo" => "required|dimensions:width=1024,height=1024",
            ]);
            if ($validator->fails()) {
                return redirect()->route('category.create')->withErrors($validator)->withInput($request->all());
            }
            $request['logo_preview'] = $encoded_image;
            $create['logo'] = $encoded_image;
        }//else {
//            if ($request->filled('logo_preview')) {
//                $create['logo'] = $request['logo_preview'];
//            }

      //  }
        if ($request->has('channels')) {
            foreach ($request['channels'] as $key => $channel) {
                $create['channels'][$key]['channel_id'] = $channel;
                $create['channels'][$key]['priority'] = $key + 1;
            }

        }
//        return $create;
        $url = config("config.channel_base_url") . "/admin/categories/$id";
        $data = RemoteCallFacade::storeWithoutOauth($url, $create);
        if ($data['status'] != 200) {
            if ($data['status'] == 422) {
                return redirect()->route('category.edit', $id)->withErrors($data['message']['message'])->withInput($request->all());

            }
            return redirect()->route('category.edit', $id)->withErrors(["Something went wrong. Please try again later!!!"])->withInput($request->all());

        }
        return redirect()->route('category.index')->with(["success" => "Category Updated Successfully."]);
    }

    public function delete($id)
    {
        try {
            $url = config('config.channel_base_url') . "/admin/categories/$id";

            $data = RemoteCallFacade::deleteSpecific($url);

            if ($data["status"] != 200) {
                return "false";
            } else {
                return "true";
            }

        } catch (\Exception $ex) {
            return "false";
        }

    }
}
