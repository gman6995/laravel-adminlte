<?php

/**
 * Created by PhpStorm.
 * @author Kabina Suwal <kabina.suwal92@gmail.com>
 * Date: 21-May-18
 * Time: 12:29 PM
 */

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Facades\RemoteCallFacade;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;

class ChannelController extends Controller
{
    public function index(Request $request)
    {
        $page = isset($request['page']) ? $request['page'] : 1;
        $url = config('config.channel_base_url') . "/admin/channels?page=$page";
        $channels = RemoteCallFacade::getSpecific($url);
        //  return $channels;
        if ($channels["status"] != 200) {
            if ($channels["status"] == 204) {
                return view(config('theme.back') . 'Page.Channels');
            }
            return view(config('theme.back') . 'Page.Channels')->withErrors($channels);
//            return response()->json([
//                "status" => $channels["status"],
//                "message" => $channels["message"]["message"],
//            ], $channels["status"]);
        }
        return view(config('theme.back') . 'Page.Channels', ['channels' => $channels['message']]);

    }

    public function create()
    {
        $url = config('config.channel_base_url') . "/admin/language-server-list";
        $data = RemoteCallFacade::getSpecificWithoutToken($url);

        if ($data["status"] != 200) {
            return view(config('theme.back') . 'Page.Channels')->withErrors($data['message'] = ['message' => "Something went wrong. Please try again later"]);
        }

        $languages = $data['message']['languages']['data'];
        $servers = $data['message']['stream_servers']['data'];

        $countries = config('country.countries');

        return view(config('theme.back') . 'Page.Channel-new')->with([
            'languages' => $languages,
            'countries' => $countries,
            'servers' => $servers
        ]);
    }

    public function store(Request $request)
    {
        $create = $request->all();
        if ($request->hasFile('logo')) {
            $this->validate($request, [

                "logo" => "sometimes|mimes:png|dimensions:width=1024,height=1024|max:700",
            ]);
            $file = $request->file('logo');
            $imagePath = addslashes($file->getPathName());

            $encoded_image = "data:image;base64," . base64_encode(file_get_contents($imagePath));

            $create['logo'] = $encoded_image;
            $request['image_path'] = $encoded_image;
        } else {
            if ($request->filled('image_path')) {
                $create['logo'] = $request['image_path'];
            }

        }

        try {


            if (isset($request['tags'])) {
                $create['tags'] = array_values(array_filter($request['tags']));
            }
            if (is_null($request['enabled'])) {
                $create['enabled'] = 0;
            }

            $url = config('config.channel_base_url') . "/admin/channels";
            $data = RemoteCallFacade::storeWithoutOauth($url, $create);

            if ($data['status'] != 200) {
                return redirect()->route('channel.store')->withErrors($data['message']['message'])->withInput($request->all());
            } else {
                return redirect()->route('channel.index')->with(['success' => "Channel created successfully"]);
            }
        } catch (\Exception $ex) {
            return redirect()->route('channel.store')->withErrors(['Something went wrong. Please try again later']);
        }

    }

    public function delete($id)
    {
        try {
            $url = config('config.channel_base_url') . "/admin/channels/$id";

            $data = RemoteCallFacade::deleteSpecific($url);

            if ($data["status"] != 200) {
                return "false";
            } else {
                return "true";
            }

        } catch (\Exception $ex) {
            return "false";
        }

    }

    public function view($id)
    {
        try {
            $url = config('config.channel_base_url') . "/admin/channels/$id";

            $channel = RemoteCallFacade::getSpecific($url);
            if ($channel["status"] != 200) {
                throw new  \Exception;
            }
            return view(config('theme.back') . 'Page.Channel-single', ['channel' => $channel['message']]);
        } catch (\Exception $ex) {
            return redirect()->route('channel.index')->withErrors(["Something went wrong. Please try again later!!!"]);
        }

    }

    public function edit($id)
    {
        try {

            $url = config('config.channel_base_url') . "/admin/language-server-list";
            $data = RemoteCallFacade::getSpecificWithoutToken($url);

            if ($data["status"] != 200) {
                throw new \Exception;
            }

            $languages = $data['message']['languages']['data'];
            $servers = $data['message']['stream_servers']['data'];

            $countries = config('country.countries');
            $url = config('config.channel_base_url') . "/admin/channels/$id";

            $channel = RemoteCallFacade::getSpecific($url);
            if ($channel["status"] != 200) {
                throw new \Exception;
            }

            return view(config('theme.back') . 'Page.Channels-edit')->with([
                'languages' => $languages,
                'countries' => $countries,
                'servers' => $servers,
                'channel' => $channel['message']
            ]);
        } catch (\Exception $ex) {
            return redirect()->route('channel.index')->withErrors(["Something went wrong. Please try again later!!!"]);
        }
    }

    public function update($id, Request $request)
    {
        $update = $request->all();
        if ($request->hasFile('logo')) {
            $this->validate($request, [

                "logo" => "sometimes|mimes:png|dimensions:width=1024,height=1024|max:700",
            ]);
            $file = $request->file('logo');
            $imagePath = addslashes($file->getPathName());

            $encoded_image = "data:image;base64," . base64_encode(file_get_contents($imagePath));

            $update['logo'] = $encoded_image;
            $request['image_path'] = $encoded_image;
//        } else {
//            if ($request->filled('image_path')) {
//                $update['logo'] = $request['image_path'];
//            }
//
        }

        try {

            if (isset($request['tags'])) {
                $update['tags'] = array_values(array_filter($request['tags']));
            }
            if (is_null($request['enabled'])) {
                $update['enabled'] = 0;
            }

            $url = config('config.channel_base_url') . "/admin/channels/$id";

            $data = RemoteCallFacade::storeWithoutOauth($url, $update);

            if ($data['status'] != 200) {
                if ($data['status'] == 422) {
                    return redirect()->route('channel.update', ['id' => $id])->withErrors($data['message']['message'])->withInput($request->all());
                } else {
                    return redirect()->route('channel.update', ['id' => $id])->withErrors($data['message']['message'])->withInput($request->all());
                }

            } else {
                return redirect()->route('channel.index')->with(['success' => "Channel updated successfully"]);
            }

        } catch (\Exception $ex) {
            return redirect()->route('channel.index')->withErrors(["Something went wrong. Please try again later!!!"]);
        }

    }
}