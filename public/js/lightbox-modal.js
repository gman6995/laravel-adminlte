/*
 Info:
  Plugin: lightbox
  Version: 1.0
  Author: Sujan Byanjankar
  E-mail: byanjankarsujan@gmail.com
*/
(function ($) {

    $.fn.lightbox = function (options) {

        var defaults = $.extend({
            modal: '#imagePopup'
        }, options);

        var src = $(this).data('src');
        var modalel = $(defaults.modal);

        $(this).click(function () {
            modalel.modal("show");
            modalel.find('.modal-body img').attr('src',src);
        });
    }
}(jQuery));