function readURL(input,el) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            $('#'+el).attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}