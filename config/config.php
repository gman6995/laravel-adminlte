<?php
/**
 * Created by PhpStorm.
 * @author Kabina Suwal <kabina.suwal92@gmail.com>
 * Date: 22-May-18
 * Time: 11:27 AM
 */

if(env("APP_ENV") == "production")
{
    return array(
        "channel_base_url" => "http://new.nettv.com.np",
    );
}else{
    return array(
        "channel_base_url" => "http://localhost:8000",
    );
}